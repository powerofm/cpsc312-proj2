import System.Random
import Data.Char
import Data.List

-- Game Board Enumeration
-- 0: Empty
-- 1: Empty, flagged
-- 2: Empty, cleared
-- 3: Bomba
-- 4: Bomba, flagged
empty = 0
emptyFlagged = 1
emptyCleared = 2
mine = 3
mineFlagged = 4

boardSize = 8
-- Reasoning: safe = x < 3, flag x += 1



-- ==============================
-- PROGRAM ENTRY POINT
-- ==============================
main = do
  putStrLn "  ╔═━───────────────────━═╗"
  putStrLn "  ┃ M I N E S W E E P E R ┃"
  putStrLn "  ╚═━───────────────────━═╝"
  gen <- getStdGen
  let board = generateBoard boardSize boardSize gen
  game board boardSize



-- ==============================
-- MAIN GAME LOOP
-- ==============================
game board size = do
  printBoard board False
  tile <- inputPrompt size
  let hitMine = checkHitMine board tile
  if hitMine then do
    printBoard board True
    putStrLn "You've hit a mine! GG"
  else do
    let nextBoard = updateBoard board tile
    if checkGameWin nextBoard then do 
      printBoard nextBoard False
      putStrLn "Congratulations! You won!"
    else do
      game nextBoard size


-- Generate a game board with a 25% chance of tiles being a bomb
generateBoard :: RandomGen g => Int -> Int -> g -> [[Int]]
generateBoard w h gen = make2DList w h (randNums w h gen)
  where
    randNums w h gen = map (\x -> if x == 1 then mine else empty) (take (w * h) (randomRs (0::Int, 4::Int) gen))
    make2DList w h lst = [[lst!!((h * i) + j) | j <- [0..w-1]] | i <- [0..h-1]]



-- ==============================
-- INPUT PARSING
-- ==============================
inputPrompt size = do
  putStrLn "Enter a tile (ex. b6). To flag a tile, prefix with F (ex: Fb6)."
  input <- getLine
  let tile = inputParse input
  let valid = inputValidate size tile
  if valid then
    return tile
  else do
    putStrLn "Invalid tile."
    inputPrompt size

-- Convert an input string "colROW" or "FcolROW" to a triple (flag, col, ROW),
-- where col is a char between a-h, and row is an int between 1-8
inputParse :: [Char] -> (Bool, Int, Int)
inputParse input
  | isFlagged input = (True, extract input 1 'a', extract input 2 '1')
  | otherwise = (False, extract input 0 'a', extract input 1 '1')
  where
    isFlagged input = (input !! 0) == 'F'
    extract input index base = (ord (input !! index)) - (ord base)

-- Checks if the selected tile is within bounds of the board
inputValidate :: Int -> (Bool, Int, Int) -> Bool
inputValidate size (_, row, col) = row >= 0 && row < size && col >= 0 && col < size



-- ==============================
-- GAME LOGIC
-- ==============================
-- Checks if the selected tile was a mine
checkHitMine :: [[Int]] -> (Bool, Int, Int) -> Bool
checkHitMine board (flag, row, col)
  | flag = False
  | otherwise = (board !! row !! col) >= mine

-- Checks if all the mines are flagged and all the empty tiles have been cleared
checkGameWin :: [[Int]] -> Bool
checkGameWin board = length (filter (\n -> (length (filter (\m -> m /= emptyCleared && m /= mineFlagged) n)) /= 0) board) == 0

-- Update the game board with the new cleared/flagged tile
updateBoard :: [[Int]] -> (Bool, Int, Int) -> [[Int]]
updateBoard board (flag, row, col)
  | row < 0 || row >= boardSize || col < 0 || col >= boardSize = board
  | not flag && numAdjacentBombs row col board == 0 && (board !! row !! col) /= emptyCleared =
    updateBoard (
      updateBoard (
        updateBoard (
          updateBoard (
            updateBoard (
              updateBoard (
                updateBoard (
                  updateBoard (
                    replaceNth board row (
                      replaceNth (board !! row) col (
                        newTileValue (board !! row !! col) flag
                      )
                    )
                  ) (flag, row-1, col-1)
                ) (flag, row-1, col)
              ) (flag, row-1, col+1)
            ) (flag, row, col-1)
          ) (flag, row, col+1)
        ) (flag, row+1, col-1)
      ) (flag, row+1, col)
    ) (flag, row+1, col+1)
  | otherwise = replaceNth board row (
      replaceNth (board !! row) col (
        newTileValue (board !! row !! col) flag
      )
    )
  where
    newTileValue tile flag
      | flag && tile == empty = emptyFlagged
      | flag && tile >= mine = mineFlagged
      | otherwise = emptyCleared
    replaceNth (head:tail) n newVal
      | n == 0 = newVal:tail
      | otherwise = head:replaceNth tail (n-1) newVal


-- ==============================
-- BOARD PRINTING
-- ==============================
-- Prints the game board
printBoard :: [[Int]] -> Bool -> IO ()
printBoard board gg = do
    putStrLn "    1  2  3  4  5  6  7  8"
    putStrLn "  ╔═━────────────────────━═╗"
    sequence (mapWithBoardAndRow printRow board board gg)
    putStrLn "  ╚═━────────────────────━═╝"
    putStrLn "    1  2  3  4  5  6  7  8"

printRow :: [Int] -> (Int, [[Int]], Bool) -> IO ()
printRow row (index, board, gg)
  | index == 0 = do
    putStr ([chr (ord 'a' + index)] ++ " ╿")
    sequence (mapWithBoardAndCell printCell row index board gg)
    putStrLn "╿"
  | index == boardSize - 1 = do
    putStr ([chr (ord 'a' + index)] ++ " ╽")
    sequence (mapWithBoardAndCell printCell row index board gg)
    putStrLn "╽"
  | otherwise = do
    putStr ([chr (ord 'a' + index)] ++ " │")
    sequence (mapWithBoardAndCell printCell row index board gg)
    putStrLn "│"

printCell :: Int -> (Int, Int, [[Int]], Bool) -> IO ()
printCell cell (col, row, board, gg)
  | gg && (cell == mine || cell == mineFlagged) = putStr " ✘ "
  | cell == emptyFlagged || cell == mineFlagged = putStr " ⚑ "
  | cell == empty || cell == mine = putStr " ■ "
  | cell == emptyCleared && (numAdjacentBombs row col board) == 0 = putStr "   "
  | otherwise = putStr (" " ++ show (numAdjacentBombs row col board) ++ " ")

-- ==============================
-- HELPER FUNCTIONS
-- ==============================

numAdjacentBombs :: Int -> Int -> [[Int]] -> Int
numAdjacentBombs row col board =
  length (filter (\n -> n == True) [hasBomb (row-1) (col-1) board, hasBomb (row-1) (col) board, hasBomb (row-1) (col+1) board, hasBomb (row) (col-1) board, hasBomb (row) (col+1) board, hasBomb (row+1) (col-1) board, hasBomb (row+1) (col) board, hasBomb (row+1) (col+1) board])

hasBomb :: Int -> Int -> [[Int]] -> Bool
hasBomb row col board
  | row >= 0 && row < boardSize && col >= 0 && col < boardSize = (board !! row !! col) >= mine
  | otherwise = False

-- map but include the array index, the board, and whether the game is over as a tuple parameter
mapWithBoardAndRow :: (a -> (Int, [[Int]], Bool) -> b) -> [a] -> [[Int]] -> Bool -> [b]
mapWithBoardAndRow f l board gg = zipWith f l (zip3 [0..] (repeat board) (repeat gg))

mapWithBoardAndCell :: (a -> (Int, Int, [[Int]], Bool) -> b) -> [a] -> Int -> [[Int]] -> Bool -> [b]
mapWithBoardAndCell f l row board gg = zipWith f l (zip4 [0..] (repeat row) (repeat board) (repeat gg))